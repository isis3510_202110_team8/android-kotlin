package com.example.housechores


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


class MapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMyLocationClickListener, GoogleMap.OnMyLocationButtonClickListener  {

    private lateinit var map: GoogleMap
    private lateinit var location: Location
    companion object {
        const val REQUEST_CODE_LOCATION = 0
        const val earthRadiusKm: Double = 6372.8
    }

    private lateinit var database: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.maps, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        database = Firebase.database.reference
        createFragment()

    }

    fun createFragment(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableMyLocation()
        map.setOnMyLocationClickListener(this)
        map.setOnMyLocationButtonClickListener(this)
    }

    fun createMarker(lat: Double, lng: Double, name: String, color: Int, dist: Double){
        var distRounded = String.format("%.3f", dist).toDouble()
        var marcador = MarkerOptions().position(LatLng(lat, lng)).title(name + ": " + distRounded + "km")
        when(color){
            0 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            1 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
            2 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
            3 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            4 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
            5 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
        }
        map.addMarker(marcador)
    }


    private fun isLocationPermissionGranted():Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }


    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )) {
            Toast.makeText(
                requireContext(),
                "Change your device settings to enable the gps function",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    requireContext(),
                    "Change your device settings to enable the gps function",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {}
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(
                requireContext(),
                "Change your device settings to enable the gps function",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onMyLocationClick(p0: Location) {
        //Borrar markers viejos
        location = p0
        println("" + location.latitude + "," + location.longitude)
        map.clear()
        database = Firebase.database.reference
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var uId = 1
                var counter = 0
                while (snapshot.hasChild("users/$uId")) {
                    val latitud = snapshot.child("users/$uId").child("latitud").value as Double
                    val longitud = snapshot.child("users/$uId").child("longitud").value as Double
                    val lastname =
                        (if (snapshot.child("users/$uId").child("lastname").value == null) {
                            snapshot.child("users/$uId").child("last_name").value as String
                        } else {
                            snapshot.child("users/$uId").child("lastname").value as String
                        })
                    val name = (if (snapshot.child("users/$uId").child("name").value == null) {
                        snapshot.child("users/$uId").child("first_name").value as String
                    } else {
                        snapshot.child("users/$uId").child("name").value as String
                    })
                    val fullname = "$name $lastname"

                    var dist = haversine(
                        LatLng(latitud, longitud), LatLng(
                            location.latitude,
                            location.longitude
                        )
                    )
                    if (dist < 0.1) {
                        createMarker(latitud, longitud, fullname, 0, dist)
                        saveMarker(latitud, longitud, fullname, 0, dist)
                        counter++
                    } else if (dist < 0.2) {
                        createMarker(latitud, longitud, fullname, 1, dist)
                        saveMarker(latitud, longitud, fullname, 1, dist)
                        counter++
                    } else if (dist < 0.5) {
                        createMarker(latitud, longitud, fullname, 2, dist)
                        saveMarker(latitud, longitud, fullname, 2, dist)
                        counter++
                    } else if (dist < 1) {
                        createMarker(latitud, longitud, fullname, 3, dist)
                        saveMarker(latitud, longitud, fullname, 3, dist)
                        counter++
                    } else if (dist < 2) {
                        createMarker(latitud, longitud, fullname, 4, dist)
                        saveMarker(latitud, longitud, fullname, 4, dist)
                        counter++
                    } else if (dist < 5) {
                        createMarker(latitud, longitud, fullname, 5, dist)
                        saveMarker(latitud, longitud, fullname, 5, dist)
                        counter++
                    }

                    uId++
                }
                if (counter == 0) {
                    Toast.makeText(
                        requireContext(),
                        "There aren't nearby clients",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (counter == 1) {
                    Toast.makeText(
                        requireContext(),
                        "There are ${counter} nearby clients",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "There are ${counter} nearby clients",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

    }

    fun haversine(client: LatLng, worker: LatLng): Double {
        val dLat = Math.toRadians(client.latitude - worker.latitude)
        val dLon = Math.toRadians(client.longitude - worker.longitude)
        val originLat = Math.toRadians(worker.latitude)
        val destinationLat = Math.toRadians(client.latitude)

        val a = Math.pow(Math.sin(dLat / 2), 2.toDouble()) + Math.pow(
            Math.sin(dLon / 2),
            2.toDouble()
        ) * Math.cos(originLat) * Math.cos(destinationLat)
        val c = 2 * Math.asin(Math.sqrt(a))
        return earthRadiusKm * c
    }



    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(
            requireContext(),
            "Click on your position to find nearby clients",
            Toast.LENGTH_SHORT
        ).show()
        return false
    }


    private fun saveMarker(lat: Double, lng: Double, name: String, color: Int, dist: Double){
       // val sharedPreferences = sharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
    }
}