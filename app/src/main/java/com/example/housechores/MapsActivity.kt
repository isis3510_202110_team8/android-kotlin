package com.example.housechores

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.housechores.databinding.ActivityMapsBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlin.math.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var location: Location
    private var loaded: Boolean = false
    private var ranks: DoubleArray = doubleArrayOf(0.1,0.2,0.5,1.0,2.0,5.0)
    private lateinit var dialogBuilder: AlertDialog.Builder
    private lateinit var dialog: AlertDialog

    companion object {
        const val REQUEST_CODE_LOCATION = 0
        const val earthRadiusKm: Double = 6372.8
    }

    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        findViewById<Button>(R.id.reload).setOnClickListener{
            findClients(map.myLocation)
        }

        findViewById<Button>(R.id.ranks).setOnClickListener{
            rankPopUp()
        }

    }

    private fun rankPopUp(){
        dialogBuilder = AlertDialog.Builder(this)
        val rankForm = layoutInflater.inflate(R.layout.ranks_form,null)

        rankForm.findViewById<EditText>(R.id.editGreenDist).setText(""+ranks[0])
        rankForm.findViewById<EditText>(R.id.editYellowDist).setText(""+ranks[1])
        rankForm.findViewById<EditText>(R.id.editOrangeDist).setText(""+ranks[2])
        rankForm.findViewById<EditText>(R.id.editRedDist).setText(""+ranks[3])
        rankForm.findViewById<EditText>(R.id.editVioletDist).setText(""+ranks[4])
        rankForm.findViewById<EditText>(R.id.editBlueDist).setText(""+ranks[5])

        dialogBuilder.setView(rankForm)
        dialog = dialogBuilder.create()
        dialog.show()

        rankForm.findViewById<Button>(R.id.cancelRank).setOnClickListener{
            dialog.dismiss()
        }
        rankForm.findViewById<Button>(R.id.submitRank).setOnClickListener{
            ranks[0]=rankForm.findViewById<EditText>(R.id.editGreenDist).text.toString().toDouble()
            ranks[1]=rankForm.findViewById<EditText>(R.id.editYellowDist).text.toString().toDouble()
            ranks[2]=rankForm.findViewById<EditText>(R.id.editOrangeDist).text.toString().toDouble()
            ranks[3]=rankForm.findViewById<EditText>(R.id.editRedDist).text.toString().toDouble()
            ranks[4]=rankForm.findViewById<EditText>(R.id.editVioletDist).text.toString().toDouble()
            ranks[5]=rankForm.findViewById<EditText>(R.id.editBlueDist).text.toString().toDouble()
            dialog.dismiss()
        }

    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        enableMyLocation()
        loadData()
    }


    fun createMarker(lat: Double, lng: Double, name: String, color: Int, dist: Double){
        val distRounded = String.format("%.3f", dist).toDouble()
        val marcador = MarkerOptions().position(LatLng(lat, lng)).title(name + ": " + distRounded + "km")
        when(color){
            0 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            1 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
            2 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
            3 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
            4 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
            5 -> marcador.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
        }
        map.addMarker(marcador)
    }


    private fun isLocationPermissionGranted():Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }


    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )) {
            Toast.makeText(
                this,
                "Change your device settings to enable the gps function",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MapsFragment.REQUEST_CODE_LOCATION
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            MapsFragment.REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    this,
                    "Change your device settings to enable the gps function",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {}
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(
                this,
                "Change your device settings to enable the gps function",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun findClients(p0: Location) {
        if(!checkConnection()){
            Toast.makeText(
                this,
                "Connect to internet to use the nearby clients service",
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        loaded = true
        location = p0
        println("" + location.latitude + "," + location.longitude)
        map.clear()
        database = Firebase.database.reference
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var uId = 1
                var counter = 0
                while (snapshot.hasChild("users/$uId")) {
                    val latitud = snapshot.child("users/$uId").child("latitud").value as Double
                    val longitud = snapshot.child("users/$uId").child("longitud").value as Double
                    val lastname =
                        (if (snapshot.child("users/$uId").child("lastname").value == null) {
                            snapshot.child("users/$uId").child("last_name").value as String
                        } else {
                            snapshot.child("users/$uId").child("lastname").value as String
                        })
                    val name = (if (snapshot.child("users/$uId").child("name").value == null) {
                        snapshot.child("users/$uId").child("first_name").value as String
                    } else {
                        snapshot.child("users/$uId").child("name").value as String
                    })
                    val fullname = "$name $lastname"

                    val dist = haversine(
                        LatLng(latitud, longitud), LatLng(
                            location.latitude,
                            location.longitude
                        )
                    )

                    when {
                        dist < ranks[0] -> {
                            createMarker(latitud, longitud, fullname, 0, dist)
                            saveMarker(latitud, longitud, fullname, 0, dist, counter)
                            counter++
                        }
                        dist < ranks[1] -> {
                            createMarker(latitud, longitud, fullname, 1, dist)
                            saveMarker(latitud, longitud, fullname, 1, dist, counter)
                            counter++
                        }
                        dist < ranks[2] -> {
                            createMarker(latitud, longitud, fullname, 2, dist)
                            saveMarker(latitud, longitud, fullname, 2, dist, counter)
                            counter++
                        }
                        dist < ranks[3] -> {
                            createMarker(latitud, longitud, fullname, 3, dist)
                            saveMarker(latitud, longitud, fullname, 3, dist, counter)
                            counter++
                        }
                        dist < ranks[4] -> {
                            createMarker(latitud, longitud, fullname, 4, dist)
                            saveMarker(latitud, longitud, fullname, 4, dist, counter)
                            counter++
                        }
                        dist < ranks[5] -> {
                            createMarker(latitud, longitud, fullname, 5, dist)
                            saveMarker(latitud, longitud, fullname, 5, dist, counter)
                            counter++
                        }
                    }

                    uId++
                }
                callNearby(counter)
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

    }

    fun callNearby(counter: Int){
        saveCounter(counter)
        when (counter) {
            0 -> {
                Toast.makeText(
                    this,
                    "There aren't nearby clients",
                    Toast.LENGTH_SHORT
                ).show()
            }
            1 -> {
                Toast.makeText(
                    this,
                    "There are $counter nearby clients",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {
                Toast.makeText(
                    this,
                    "There are $counter nearby clients",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun haversine(client: LatLng, worker: LatLng): Double {
        val dLat = Math.toRadians(client.latitude - worker.latitude)
        val dLon = Math.toRadians(client.longitude - worker.longitude)
        val originLat = Math.toRadians(worker.latitude)
        val destinationLat = Math.toRadians(client.latitude)

        val a = sin(dLat / 2).pow(2.toDouble()) + sin(dLon / 2).pow(2.toDouble()) * cos(originLat) * cos(destinationLat)
        val c = 2 * asin(sqrt(a))
        return MapsFragment.earthRadiusKm * c
    }

    private fun saveMarker(lat: Double, lng: Double, name: String, color: Int, dist: Double, id: Int){
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putFloat("lat$id", lat.toFloat())
            putFloat("lng$id", lng.toFloat())
            putString("name$id", name)
            putInt("color$id", color)
            putFloat("dist$id", dist.toFloat())
        }.apply()
    }

    private fun saveCounter(counter: Int){
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putInt("counter", counter)
        }.apply()
    }

    private fun loadData(){
        loaded = true
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val counter = sharedPreferences.getInt("counter", 0)
        var i = 0
        while(i<counter){
            val lat = sharedPreferences.getFloat("lat$i", 0.0F).toDouble()
            val lng = sharedPreferences.getFloat("lng$i", 0.0F).toDouble()
            val name = sharedPreferences.getString("name$i", null)
            val color = sharedPreferences.getInt("color$i", -1)
            val dist = sharedPreferences.getFloat("dist$i", 0.0F).toDouble()
            if(lat!=0.0 && lng!=0.0 && !name.isNullOrBlank() && color!=-1 && dist!=0.0){
                createMarker(lat,lng,name,color,dist)
            }
            i++
        }
        Toast.makeText(
            this,
            "The markers are from previous sessions, click on your position to reload",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun checkConnection():Boolean{
        val manager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = manager.activeNetworkInfo

        if(null != networkInfo){
            return networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE
        }

        return false
    }


}