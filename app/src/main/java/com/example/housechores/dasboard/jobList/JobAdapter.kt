package com.example.housechores.dasboard.jobList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.housechores.R
import com.example.housechores.dasboard.data.Job


class JobAdapter(private val onClick: (Job)->Unit):
    ListAdapter<Job, JobAdapter.JobViewHolder>(JobDiffCallback){


    class JobViewHolder(itemView: View, val onClick: (Job) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val jobDCategoryTV: TextView = itemView.findViewById(R.id.category_tv)
        private val jobDescription: TextView = itemView.findViewById(R.id.job_description_tv)
        private val jobLocation: TextView = itemView.findViewById(R.id.location_tv)
        private val jobDate: TextView = itemView.findViewById(R.id.date_tv)
        private val jobPriceTV: TextView = itemView.findViewById(R.id.price_tv)
        private val jobClientTV: TextView = itemView.findViewById(R.id.client_tv)
        private var currentJob: Job? = null

        init {
            itemView.setOnClickListener {
                currentJob?.let {
                    onClick(it)
                }
            }
        }

        /* Jobs variables and its values. */
        fun bind(job: Job) {
            currentJob = job
            jobDCategoryTV.text= job.category
            jobDescription.text  = job.service
            jobLocation.text = job.lat.toString()+job.long.toString()
            jobDate.text = job.day
            jobClientTV.text = job.client
            jobPriceTV.text =  job.price.toString()

        }
    }

    /* Creates and inflates view and return JobViewHolder. */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.job_item, parent, false)
        return JobViewHolder(view, onClick)
    }

    /* Gets current job and uses it to bind view. */
    override fun onBindViewHolder(holder: JobViewHolder, position: Int) {
        val job = getItem(position)
        holder.bind(job)

    }
}

object JobDiffCallback : DiffUtil.ItemCallback<Job>() {
    override fun areItemsTheSame(oldItem: Job, newItem: Job): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Job, newItem: Job): Boolean {
        return oldItem.client == newItem.client
    }
}

