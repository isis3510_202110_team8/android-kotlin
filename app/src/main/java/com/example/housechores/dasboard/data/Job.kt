package com.example.housechores.dasboard.data

data class Job (
    val client: String,
    val category: String,
    val day: String,
    val hour: String,
    val lat: Double,
    val long: Double,
    val paymentMethod: String,
    val address: String,
    val price: Double,
    val service: String,
    val isCompleted: String
        )
