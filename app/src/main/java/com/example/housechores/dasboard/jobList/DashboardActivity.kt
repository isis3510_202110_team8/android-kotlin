package com.example.housechores.dasboard.jobList
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.housechores.MessagesActivity
import com.example.housechores.ProfileActivity
import com.example.housechores.R
import com.example.housechores.dasboard.data.Job
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.text.NumberFormat

class DashboardActivity : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    private val jobsListViewModel by viewModels<JobListViewModel> {
        JobListViewModelFactory(this)
    }

    private fun saveJob(category : String, service : String, lat: Double, long: Double, client : String, day : String, price: Double) {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        /**
         * Se encarga de hacer que recycler view funcione haciendo data binding
         */
        val jobAdapter = JobAdapter { }
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
        recyclerView.adapter = jobAdapter
        jobsListViewModel.jobLiveData.observe(this, {
            it?.let {
                jobAdapter.submitList(it as MutableList<Job>)
            }
        })

        /**
         * Para mostrar el precio de los servicios
         */
        val buttonPrice = findViewById<Button>(R.id.showPrice)
        buttonPrice.setOnClickListener{

            averagePriceCalc()
        }
        /**
         * Navegacion en la navegation bar
         */
        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.dashboard
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.profile -> {
                    if (isOnline()) {
                        val intent = Intent(this, ProfileActivity::class.java).apply {
                        }
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                R.id.dashboard -> {
                    if (isOnline()) {
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                R.id.messages -> {
                    if (isOnline()) {
                        val intent = Intent(this, MessagesActivity::class.java).apply {
                        }
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                else -> false
            }
        }
    }

    private fun averagePriceCalc() {
        val savedCategory:String
        val email:String?
        val sharedPreferences: SharedPreferences
        val current = Firebase.auth.currentUser
        var average:String?
        if(current != null){
            email = current.email
        }else
        {
            email="plomero70@plomeros.com"
        }
        sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        savedCategory = sharedPreferences.getString("cat$email", null).toString()
        println(savedCategory)
        val scope = CoroutineScope(Dispatchers.IO)
        database = Firebase.database.reference
        scope.launch{
            try {
                // [START post_value_event_listener]0
                val postListener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val post = dataSnapshot.child("contratos")
                        var precio=0.0
                        var contador=0
                        for(chil in post.getChildren()){
                            if(chil.child("category").getValue().toString().capitalize().equals(savedCategory.capitalize())){
                                contador=contador+1
                                val valor=chil.child("precio").getValue().toString().toDouble()
                                precio = precio+valor
                            }
                        }
                        val formatter: NumberFormat = DecimalFormat("#,###")
                        val currency = formatter.format(precio/contador)
                        average ="$ "+currency +"COP" //precioo
                        showAvgPrice(average, savedCategory.toString())
                    }
                    override fun onCancelled(databaseError: DatabaseError) {
                        // Getting Post failed, log a message
                        Log.w( "loadPost:onCancelled", databaseError.toException())
                    }
                }


                database.addValueEventListener(postListener)
            }catch (e: Exception){
                print("error retrieving data")
            }
        }


        // [END post_value_event_listener]
    }
    //Show the average price of the services other workers of the same category charge
    private fun showAvgPrice(numero: String?, category: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Average price")
        builder.setMessage("The average price for the: "+ category+ " services is:" + numero)
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun isOnline(): Boolean {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun showNoConnection() {
        Toast.makeText(
            applicationContext,
            "Impossible action: please check your connection and try again.",
            Toast.LENGTH_SHORT
        ).show()
    }
}