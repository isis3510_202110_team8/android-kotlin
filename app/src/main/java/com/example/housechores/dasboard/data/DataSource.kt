package com.example.housechores.dasboard.data

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class DataSource(resources: Resources) {

    private lateinit var database: DatabaseReference
    val scope = CoroutineScope(Dispatchers.IO)

    /*
    *Devuelve la lista de trabajos hechos por el trabajador.
     */
    fun getJobList(): ArrayList<Job> {
        val currentUser = Firebase.auth.currentUser
        database = Firebase.database.reference
        val jobs = ArrayList<Job>()
        var post: DataSnapshot
        var client : String
        var category:String
        var day : String
        var hour : String
        var lat : Double
        var long : Double
        var paymentMethod:String
        var address:String
        var price : Double
        var service:String
        var completed:String
        var job: Job
        scope.launch{
            try {
                if (currentUser != null) {
                    database.addListenerForSingleValueEvent(object : ValueEventListener {
                        val email = currentUser.email
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val route = snapshot.child("workers")
                            for(workers in route.getChildren()){
                                if (workers.child("email").value == email) {
                                    post = workers.child("contratos")
                                    for(chil in post.getChildren()){
                                        client = chil.child("cliente").value.toString()
                                        category = chil.child("category").value.toString()
                                        day = chil.child("dia").value.toString()
                                        hour = chil.child("hora").value.toString()
                                        lat = chil.child("latitud").value.toString().toDouble()
                                        long = chil.child("longitud").value.toString().toDouble()
                                        paymentMethod = chil.child("medio_de_pago").value.toString()
                                        address = chil.child("address").value.toString()
                                        price = chil.child("precio").value.toString().toDouble()
                                        service = chil.child("service").value.toString()
                                        completed = chil.child("completed").value.toString()
                                        job =  Job(client,category, day, hour, lat, long, paymentMethod, address, price, service, completed)
                                        jobs.add(job)
                                    }
                                }
                            }
                        }
                        override fun onCancelled(error: DatabaseError) {
                        }
                    })
                }
            }catch (e: Exception){
                print("error retrieving data")
            }
        }
        print("consuto base de datos")
        return jobs
    }
    private val jobs = getJobList()
    private val liveJobList = MutableLiveData(jobs)

    /*
    Develve la lista de trabajos en formato live data
     */
    fun getList(): MutableLiveData<ArrayList<Job>>
    {
        return liveJobList
    }


    companion object {
        private var INSTANCE: DataSource? = null

        fun getDataSource(resources: Resources): DataSource {
            return synchronized(DataSource::class) {
                val newInstance = INSTANCE ?: DataSource(resources)
                INSTANCE = newInstance
                newInstance
            }
        }
    }



}
