package com.example.housechores

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

class SignUpOneActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_one)
        val email = findViewById<EditText>(R.id.workerEmail)
        val password = findViewById<EditText>(R.id.workerPassword)
        val spinner: Spinner = findViewById(R.id.drop_sign)
        val nextButton = findViewById<Button>(R.id.button_next)

        val name = intent.getStringExtra("Name")
        val age = intent.getStringExtra("Age")
        val lastName = intent.getStringExtra("LastName")
        val phone = intent.getStringExtra("Phone")

        email.setText(intent.getStringExtra("Email"))
        password.setText(intent.getStringExtra("Password"))
        spinner.setSelection(intent.getIntExtra("nType", 0))

        nextButton.setOnClickListener{
            val type = spinner.selectedItem
            val ntype = spinner.selectedItemId.toInt()
            when (val validate= validation(email.text.toString(), password.text.toString())) {
                "correct" -> {
                    val intent = Intent(this, SignUpTwoActivity::class.java).apply {
                        putExtra("Email",email.text.toString() )
                        putExtra("Password",password.text.toString() )
                        putExtra("Type",type.toString() )
                        putExtra("nType", ntype )

                        putExtra("Name",name)
                        putExtra("Age",age )
                        putExtra("LastName",lastName )
                        putExtra("Phone", phone )
                    }
                    startActivity(intent)
                }
                "email" -> {
                    Snackbar.make(it, "The field $validate is incorrect", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
                "password" -> {
                    Snackbar.make(
                        it,
                        "Password must contain at least 8 characters including a number and a letter",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                }
            }
        }
    }

    private fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    private fun isValidPassword(target: CharSequence): Boolean {
        var valid = true
        // Password policy check
        // Password should be minimum minimum 8 characters long
        if (target.length < 8) {
            valid = false
        }
        // Password should contain at least one number
        val exp = ".*[0-9].*"
        val pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(target)
        if (!matcher.matches()) {
            valid = false
        }

        return valid
    }

    private fun validation(
        email: String,
        password: String
    ): String {
        var res = "correct"
        if (!isValidEmail(email)) {
            res = "email"
        } else if (!isValidPassword(password)) {
            res = "password"
        }
        return res
    }
}