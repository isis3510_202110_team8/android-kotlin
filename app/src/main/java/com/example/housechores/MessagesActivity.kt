package com.example.housechores

import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.housechores.dasboard.jobList.DashboardActivity
import com.google.android.material.bottomnavigation.BottomNavigationView

class MessagesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messages)

        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottom_navigation)

        bottomNavigationView.selectedItemId = R.id.messages
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.profile -> {
                    if (isOnline()) {
                        val intent = Intent(this, ProfileActivity::class.java).apply {
                        }
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                R.id.dashboard -> {
                    if (isOnline()) {
                        val intent = Intent(this, DashboardActivity::class.java).apply {
                        }
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                R.id.messages -> {
                    if (isOnline()) {
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                else -> false
            }
        }
    }

    private fun isOnline(): Boolean {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun showNoConnection() {
        Toast.makeText(
            applicationContext,
            "Impossible action: please check your connection and try again.",
            Toast.LENGTH_LONG
        ).show()
    }
}