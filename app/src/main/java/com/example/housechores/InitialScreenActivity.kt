package com.example.housechores

import android.app.AlertDialog
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class InitialScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial_screen)
        val signUpButton = findViewById<Button>(R.id.button_sign_up)
        signUpButton.setOnClickListener {
            signUpPressed()
        }
        val loginButton = findViewById<Button>(R.id.button_login)
        loginButton.setOnClickListener {
            logInPressed()
        }
    }
    private fun isOnline(): Boolean {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun showNoConnection() {
        Toast.makeText(
            applicationContext,
            "Connectivity error: please check your connection and try again.",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun logInPressed() {
        if (isOnline()) {
            val intent = Intent(this, LoginActivity::class.java).apply {
            }
            startActivity(intent)
        } else {
            showNoConnection()
        }
    }

    private fun signUpPressed() {
        if (isOnline()) {
            val intent = Intent(this, SignUpOneActivity::class.java).apply {
            }
            startActivity(intent)
        } else {
            showNoConnection()
        }
    }
}