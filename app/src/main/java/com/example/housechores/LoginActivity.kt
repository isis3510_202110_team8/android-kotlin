package com.example.housechores

import android.app.AlertDialog
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_actvity)
        val workerEmail = findViewById<EditText>(R.id.workerEmail)
        val workerPassword = findViewById<EditText>(R.id.workerPassword)
        val loginButton = findViewById<Button>(R.id.button_next)
        loginButton.setOnClickListener {
            loginPressed(workerEmail, workerPassword)
        }
    }

    private fun loginPressed(workerEmail: EditText, workerPassword: EditText) {
        if (workerEmail.text.isNotEmpty() && workerPassword.text.isNotEmpty()) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(
                workerEmail.text.toString(),
                workerPassword.text.toString()
            ).addOnCompleteListener {
                if (it.isSuccessful) {
                    println("User exists")
                    val intent = Intent(this, ProfileActivity::class.java).apply {
                    }
                    startActivity(intent)
                } else {
                    if (isOnline()) {
                        println(it.result)
                        println("Error while login")
                        showAlert()
                    } else {
                        showNoConnection()
                    }
                }
            }
        }
    }

    private fun isOnline(): Boolean {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun showAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Login error")
        builder.setMessage("An error has occurred, check your email and password")
        builder.setPositiveButton("Accept", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showNoConnection() {
        Toast.makeText(applicationContext,"Please check your connection and try again.",Toast.LENGTH_LONG).show();
    }
}