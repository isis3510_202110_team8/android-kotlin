package com.example.housechores

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.housechores.dasboard.jobList.DashboardActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class ProfileActivity : AppCompatActivity() {

    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val logoutButton = findViewById<Button>(R.id.btn_log_out)
        logoutButton.setOnClickListener {
            logOutPressed()
        }

        //Analytics Event
        val analytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString("message", "Successful Firebase Integration")
        analytics.logEvent("InitialScreen", bundle)

        findViewById<ImageView>(R.id.imageProfile).setImageResource(R.drawable.ic_baseline_person_24)
        loadWorkerData()
        navigation()
        findViewById<Button>(R.id.buttonLocation).setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java).apply {
            }
            startActivity(intent)
        }
    }

    private fun navigation() {
        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.profile
        //Bottom bar navigation
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.profile -> {
                    if (isOnline()) {
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                R.id.dashboard -> {
                    if (isOnline()) {
                        val intent = Intent(this, DashboardActivity::class.java).apply {
                        }
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                R.id.messages -> {
                    if (isOnline()) {
                        val intent = Intent(this, MessagesActivity::class.java).apply {
                        }
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        true
                    } else {
                        showNoConnection()
                        false
                    }
                }
                else -> false
            }
        }
    }

    private fun isOnline(): Boolean {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun showNoConnection() {
        Toast.makeText(
            applicationContext,
            "Impossible action: please check your connection and try again.",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun logOutPressed() {
        val intent = Intent(this, InitialScreenActivity::class.java).apply {
        }
        startActivity(intent)
    }

    private fun saveWorker(email: String, lastName: String, name: String, category: String) {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply {
            putString("last$email", lastName)
            putString("name$email", name)
            putString("cat$email", category)
        }.apply()
    }

    private fun loadWorker(email: String?): Array<String?> {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val savedLastName = sharedPreferences.getString("last$email", null)
        val savedName = sharedPreferences.getString("name$email", null)
        val savedCategory = sharedPreferences.getString("cat$email", null)
        return arrayOf(savedLastName, savedName, savedCategory)
    }

    private fun loadWorkerData() {
        val currentUser = Firebase.auth.currentUser
        database = Firebase.database.reference
        if (currentUser != null) {
            val email: String? = currentUser.email
            var category = ""
            var edad: String
            var lastname = ""
            var name = ""
            var phone: String
            val worker = loadWorker(email)
            if (worker[0] != null) {
                lastname = worker[0].toString()
                name = worker[1].toString()
                category = worker[2].toString()
                "$name $lastname".also {
                    findViewById<TextView>(R.id.nameProfile).text = it
                }
                findViewById<TextView>(R.id.Service).text = category
            } else {
                database.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var workerId = 1
                        while (snapshot.hasChild("workers/$workerId")) {
                            if (snapshot.child("workers/$workerId").child("email").value == email) {
                                category = snapshot.child("workers/$workerId")
                                    .child("category").value as String
                                edad =
                                    snapshot.child("workers/$workerId")
                                        .child("edad").value.toString()
                                lastname = (if (snapshot.child("workers/$workerId")
                                        .child("lastname").value == null
                                ) {
                                    snapshot.child("workers/$workerId")
                                        .child("last_name").value as String
                                } else {
                                    snapshot.child("workers/$workerId")
                                        .child("lastname").value as String
                                })
                                name = (if (snapshot.child("workers/$workerId")
                                        .child("name").value == null
                                ) {
                                    snapshot.child("workers/$workerId")
                                        .child("first_name").value as String
                                } else {
                                    snapshot.child("workers/$workerId")
                                        .child("name").value as String
                                })
                                phone =
                                    snapshot.child("workers/$workerId")
                                        .child("phone").value as String
                                break
                            }
                            workerId++
                        }
                        "$name $lastname".also {
                            findViewById<TextView>(R.id.nameProfile).text = it
                        }
                        findViewById<TextView>(R.id.Service).text = category
                        if (email != null) {
                            saveWorker(email, lastname, name, category)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                    }
                })
            }
        }
    }
}