package com.example.housechores

import android.app.AlertDialog
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


class SignUpTwoActivity : AppCompatActivity() {

    override fun onBackPressed() {
        val name = findViewById<EditText>(R.id.workerName)
        val age = findViewById<EditText>(R.id.workerAge)
        val lastName = findViewById<EditText>(R.id.workerLastName)
        val phone = findViewById<EditText>(R.id.workerPhone)
        val email = intent.getStringExtra("Email")
        val password = intent.getStringExtra("Password")
        val ntype = intent.getIntExtra("nType", 0)
        val intent = Intent(this, SignUpOneActivity::class.java).apply {
            putExtra("Name", name.text.toString())
            putExtra("Age", age.text.toString())
            putExtra("LastName", lastName.text.toString())
            putExtra("Phone", phone.text.toString())
            putExtra("Email", email)
            putExtra("Password", password)
            putExtra("nType", ntype)
        }
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_two)
        auth = Firebase.auth
        val intent = intent
        val signButton = findViewById<Button>(R.id.button_sign_last)
        val name = findViewById<EditText>(R.id.workerName)
        val age = findViewById<EditText>(R.id.workerAge)
        val lastName = findViewById<EditText>(R.id.workerLastName)
        val phone = findViewById<EditText>(R.id.workerPhone)
        val email = intent.getStringExtra("Email")
        val password = intent.getStringExtra("Password")
        val type = intent.getStringExtra("Type")

        name.setText(intent.getStringExtra("Name"))
        age.setText(intent.getStringExtra("Age"))
        lastName.setText(intent.getStringExtra("LastName"))
        phone.setText(intent.getStringExtra("Phone"))

        signButton.setOnClickListener {
            val validate =
                validation(
                    name.text.toString(),
                    lastName.text.toString(),
                    phone.text.toString()
                )
            when (validate) {
                "correct" -> {
                    createAccount(
                        name.text.toString(),
                        lastName.text.toString(),
                        email.toString(),
                        password.toString(),
                        phone.text.toString(),
                        type.toString(),
                        age.text.toString()
                    )
                }
                "name" -> {
                    Snackbar.make(
                        it,
                        "The field $validate must have the size of a minimum of 3",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                }
                "lastname" -> {
                    Snackbar.make(
                        it,
                        "The field $validate must have the size of a minimum of 2",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                }
                "phone" -> {
                    Snackbar.make(
                        it,
                        "The field $validate must have the size of a minimum of 10",
                        Snackbar.LENGTH_LONG
                    )
                        .setAction("Action", null).show()
                }
            }
        }
    }

    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    @IgnoreExtraProperties
    data class Worker(
        val category: String? = null,
        val edad: String? = null,
        val name: String? = null,
        val lastname: String? = null,
        val email: String? = null,
        val phone: String? = null
    ) {
        // Null default values create a no-argument default constructor, which is needed
        // for deserialization from a DataSnapshot.
    }

    private fun isValidName(target: CharSequence): Boolean {
        return target.trim().length > 2
    }

    private fun isValidPhone(target: CharSequence): Boolean {
        return Patterns.PHONE.matcher(target).matches() && target.trim().length >= 10
    }

    private fun validation(
        name: String,
        lastname: String,
        phone: String
    ): String {
        var res = "correct"
        if (!isValidName(name)) {
            res = "name"
        } else if (!isValidName(lastname)) {
            res = "lastname"
        } else if (!isValidPhone(phone)) {
            res = "phone"
        }
        return res
    }

    private fun writeNewWorker(
        name: String,
        lastname: String,
        email: String,
        phone: String,
        typeOfService: String,
        age: String
    ) {
        database = Firebase.database.reference
        var idWorker = 1
        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                while (snapshot.hasChild("workers/$idWorker")) {
                    idWorker++
                }
                val user = Worker(typeOfService, age, name, lastname, email, phone)
                database.child("workers").child(idWorker.toString()).setValue(user)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("firebase", error.message)
            }
        })
    }

    private fun createAccount(
        name: String,
        lastname: String,
        email: String,
        password: String,
        phone: String,
        type: String,
        age: String
    ) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    writeNewWorker(
                        name,
                        lastname,
                        email,
                        phone,
                        type,
                        age
                    )
                    println("User created")
                    showAlert(true)
                } else {
                    // If sign in fails, display a message to the user.
                    println("User already exists")
                    showAlert(false)
                }
            }
    }

    private fun showAlert(success: Boolean) {
        val builder = AlertDialog.Builder(this)
        if (isOnline() && success) {
            builder.setTitle("Success")
            builder.setMessage("The account was successfully created")
            builder.setPositiveButton("Accept") { _, _ ->
                val intent = Intent(this, InitialScreenActivity::class.java).apply {}
                startActivity(intent)
            }
        } else if (isOnline() && !success) {
            builder.setTitle("Error creating the account")
            builder.setMessage("The e-mail is already linked to another account")
            builder.setNegativeButton("Accept", null)
        } else if (!isOnline()) {
            val options = arrayOf("Accept", "Go back")
            builder.setTitle("Please check your connection and try again.")
            builder.setItems(options) { dialog, which ->
                if (which == 0) {
                    dialog.dismiss()
                } else {
                    val intent = Intent(this, InitialScreenActivity::class.java).apply {}
                    startActivity(intent)
                }
            }
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun isOnline(): Boolean {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}